#ifndef EURECA_DOCKING_H
#define EURECA_DOCKING_H

#include <ros/ros.h>

//#include <tf/tf.h>
#include <tf/transform_listener.h>

#include <laser_geometry/laser_geometry.h>

#include <geometry_msgs/Twist.h>
#include <sensor_msgs/LaserScan.h>
#include <std_msgs/String.h>

#include <laser_line_extraction/LineSegment.h>
#include <laser_line_extraction/LineSegmentList.h>

#include <actionlib/server/simple_action_server.h>
#include <eureca_docking/DockingAction.h>

#include <visualization_msgs/Marker.h>

namespace eureca
{
class Docking
{
public:
  Docking();

  ~Docking();

  // TODO inserimento mantenedo allineamento
  bool docking();

  bool approach();

  /**
   * @brief Simple recovery routine
   * @return
   */
  bool recovery();

  /**
   * @brief Drive forward a specified distance based on odometry information
   * @param distance
   */
  bool driveForwardOdom(double distance);

  /**
   * @brief Rotate of a specified angle based on odometry information
   * @param angle
   */
  bool rotateOdom(double angle);

private:
  struct Segment
  {
    struct Point
    {
      double x;
      double y;
    };
    Point p1;
    Point p2;
  };

  std::string name_;

  ros::NodeHandle nh_;

  ros::Publisher velocity_pub_;
  ros::Subscriber laser_sub_;
  ros::Subscriber lines_sub_;
  tf::TransformListener tf_listener_;

  ros::Publisher marker_pub_;

  std::string action_name_ = "docking";
  actionlib::SimpleActionServer<eureca_docking::DockingAction> as_;

  laser_geometry::LaserProjection projector_;

  std::string base_frame_;
  std::string camera_frame_; //"/camera_optical"
  std::string tag_frame_;
  std::string odom_frame_;
  std::string scan_topic_; //"/scan/front"
  std::string lines_topic_ = "/line_segments";

  std::vector<Segment> segments_;

  double alignment_tolerance_ = 0.05;
  double alignment_distance_ = 1.5;

  tf::StampedTransform last_tag_;
  double cart_x_ = 2.5;
  double cart_y_ = 1.5; // 1.0

  void scanCallback(const sensor_msgs::LaserScan::ConstPtr& scan);

  void
  linesCallback(const laser_line_extraction::LineSegmentList::ConstPtr& lines);

  void executeCallback();

  void publishZeroVelocity();

  void filterLines(const laser_line_extraction::LineSegmentList& lines_msg,
                   std::vector<Segment>& segments);

  bool alignmentError(double& linear_error, double& angular_error);

  void showLines(double a, double b, double c, int id);

  double getAngle(const Eigen::Vector2d& v1, const Eigen::Vector2d& v2,
                  const bool in_degree)
  {
    // Compute the actual angle
    double rad = v1.normalized().dot(v2.normalized());
    if (rad < -1.0)
      rad = -1.0;
    else if (rad > 1.0)
      rad = 1.0;
    return (in_degree ? acos(rad) * 180.0 / M_PI : acos(rad));
  }
};
}

#endif // EURECA_DOCKING_H
