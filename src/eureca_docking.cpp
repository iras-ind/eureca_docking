#include <eureca_docking/eureca_docking.h>

namespace eureca
{
Docking::Docking()
    : as_(nh_, action_name_, boost::bind(&Docking::executeCallback, this), false)
{
  ros::NodeHandle private_nh("~");

  private_nh.param("base_frame", base_frame_, std::string("/base_footprint"));
  private_nh.param("camera_frame", camera_frame_, std::string("/usb_cam"));
  private_nh.param("tag_frame", tag_frame_, std::string("/tag_0"));
  private_nh.param("odom_frame", odom_frame_, std::string("/odom"));
  private_nh.param("scan_topic", scan_topic_, std::string("/scan_front"));

  laser_sub_ = nh_.subscribe<sensor_msgs::LaserScan>(
      scan_topic_, 1, &Docking::scanCallback, this);

  velocity_pub_ = nh_.advertise<geometry_msgs::Twist>("/cmd_vel", 10);

  lines_sub_ = nh_.subscribe<laser_line_extraction::LineSegmentList>(
      lines_topic_, 1, &Docking::linesCallback, this);

  marker_pub_ =
      nh_.advertise<visualization_msgs::Marker>("visualization_marker", 10);

  as_.start();
}

Docking::~Docking() { publishZeroVelocity(); }

void Docking::scanCallback(const sensor_msgs::LaserScan::ConstPtr& scan)
{
  if (!tf_listener_.waitForTransform(
          scan->header.frame_id, base_frame_,
          scan->header.stamp +
              ros::Duration().fromSec(scan->ranges.size() *
                                      scan->time_increment),
          ros::Duration(1.0)))
  {
    return;
  }
}

void Docking::linesCallback(
    const laser_line_extraction::LineSegmentList::ConstPtr& lines_msg)
{
  segments_.clear();
  filterLines(*lines_msg, segments_);
}

void Docking::executeCallback()
{
  eureca_docking::DockingResult result;

  ROS_INFO("Approach");

  if (!approach())
  {
    ROS_ERROR("Impossible to find the cart.");
    result.result = false;
    as_.setAborted(result);
  }

  ROS_INFO("Docking");

  if (!docking())
  {
    ROS_ERROR("Impossible to allign to the cart.");
    result.result = false;
    as_.setAborted(result);
  }

  driveForwardOdom(2.0);

  ROS_INFO("DONE");

  result.result = true;
  as_.setSucceeded(result);
}

void Docking::filterLines(
    const laser_line_extraction::LineSegmentList& lines_msg,
    std::vector<Segment>& segments)
{
  tf::StampedTransform transform;

  tf_listener_.lookupTransform(base_frame_, lines_msg.header.frame_id,
                               ros::Time(0), transform);

  for (int i = 0; i < lines_msg.line_segments.size(); i++)
  {
    laser_line_extraction::LineSegment seg(lines_msg.line_segments[i]);

    tf::Vector3 start(seg.start[0], seg.start[1], 0.0);
    tf::Vector3 end(seg.end[0], seg.end[1], 0.0);

    start = transform * start;
    end = transform * end;

    // TODO testare
    // std::cout << "start: " << start.x() << " end: " << end.x() << std::endl;
    if (start.x() * end.y() - start.y() * end.x() > 0.0)
    {
      tf::Vector3 tmp = start;
      start = end;
      end = tmp;
    }

    // std::cout << "start: " << start.x() << " end: " << end.x() << std::endl;

    double tag_x = last_tag_.getOrigin().x();
    double tag_y = last_tag_.getOrigin().y();

    // CONTROLLO BOUNDING BOX

    // TODO rimuovere 0.1, usato per test senza carrello
    if (start.x() < tag_x - 0.1 || start.x() > tag_x + cart_x_ ||
        end.x() < tag_x - 0.1 || end.x() > tag_x + cart_x_)
    {
      // std::cout << "Out of bounds: x" << std::endl;
      continue;
    }

    if (fabs(start.y() - tag_y) > (cart_y_ / 2) ||
        fabs(end.y() - tag_y) > (cart_y_ / 2))
    {
      // std::cout << "Out of bounds: y" << std::endl;
      continue;
    }

    // CONTROLLO ANGOLO
    Eigen::Vector2d vec(end.x() - start.x(), end.y() - start.y());
    // vec.normalize();

    double ang = fabs(Docking::getAngle(Eigen::Vector2d(1.0, 0.0), vec, false));
    ang = std::min(ang, M_PI - ang);

    if (ang > 0.5)
    {
      continue;
    }

    /*
    std::cout << "start: " << start.x() << "," << start.y() << "," << start.z()
              << std::endl;
    std::cout << "end: " << end.x() << "," << end.y() << "," << end.z()
              << std::endl;
              */

    // std::cout << "vec: " << vec << std::endl;

    // std::cout << "ang: " << ang << std::endl;

    // Aggiungo segmento ai segmenti validi
    Segment segment;
    segment.p1.x = start.x();
    segment.p1.y = start.y();
    segment.p2.x = end.x();
    segment.p2.y = end.y();
    segments.push_back(segment);
  }
}

bool Docking::alignmentError(double& linear_error, double& angular_error)
{
  if (segments_.size() != 2)
  {
    ROS_WARN_STREAM("Wrong number of segments: " << segments_.size());
    return false;
  }

  /*.
    if (lines[0][2] < 0)
    {
      lines[0][2] *= -1;
      lines[0][3] *= -1;
    }

    if (lines[1][2] < 0)
    {
      lines[1][2] *= -1;
      lines[1][3] *= -1;
    }
    */

  std::cout << "start: " << segments_[0].p1.x << "," << segments_[0].p1.y
            << std::endl;
  std::cout << "end: " << segments_[0].p2.x << "," << segments_[0].p2.y
            << std::endl;

  // Coefficienti impliciti delle 2 rette (a, b, c)
  Eigen::Vector3d line_coeff1, line_coeff2;
  line_coeff1[0] = segments_[0].p1.y - segments_[0].p2.y;
  line_coeff1[1] = segments_[0].p2.x - segments_[0].p1.x;
  line_coeff1[2] = segments_[0].p1.x * segments_[0].p2.y -
                   segments_[0].p2.x * segments_[0].p1.y;

  line_coeff2[0] = segments_[1].p1.y - segments_[1].p2.y;
  line_coeff2[1] = segments_[1].p2.x - segments_[1].p1.x;
  line_coeff2[2] = segments_[1].p1.x * segments_[1].p2.y -
                   segments_[1].p2.x * segments_[1].p1.y;

  /*
  line_coeff2[0] = segments_[1].p2.y;
  line_coeff2[1] = -segments_[1].p2.x;
  line_coeff2[2] = segments_[1].p2.x * segments_[1].p1.y - segments_[1].p1.x +
                   segments_[1].p2.y;
                   */

  // Calcolo le due bisettrice
  Eigen::Vector3d bis_coeff;

  double ab1 = sqrt(pow(line_coeff1[0], 2.0) + pow(line_coeff1[1], 2.0));
  double ab2 = sqrt(pow(line_coeff2[0], 2.0) + pow(line_coeff2[1], 2.0));

  // std::cout << "ab1: " << ab1 << std::endl;
  // std::cout << "ab2: " << ab2 << std::endl;

  bis_coeff[0] = line_coeff1[0] / ab1 - line_coeff2[0] / ab2;
  bis_coeff[1] = line_coeff1[1] / ab1 - line_coeff2[1] / ab2;
  bis_coeff[2] = line_coeff1[2] / ab1 - line_coeff2[2] / ab2;

  // TODO Verificare e migliorare angoli
  angular_error = atan2(-bis_coeff[0], bis_coeff[1]);

  if (angular_error > M_PI / 2)
    angular_error -= M_PI;
  else if (angular_error < -M_PI / 2)
    angular_error += M_PI;

  ROS_INFO_STREAM( "R1: " << line_coeff1.transpose() );
  ROS_INFO_STREAM( "R2: " << line_coeff2.transpose() );
  ROS_INFO_STREAM( "B: "  << bis_coeff.transpose() );

  showLines(line_coeff1[0], line_coeff1[1], line_coeff1[2], 0);
  showLines(line_coeff2[0], line_coeff2[1], line_coeff2[2], 1);
  showLines(bis_coeff[0], bis_coeff[1], bis_coeff[2], 2);

  // Distanza robot (origine) bisettrice
  linear_error =
      bis_coeff[2] / sqrt(pow(bis_coeff[0], 2.0) + pow(bis_coeff[1], 2.0));

  ROS_INFO_STREAM("Linear Error: " << linear_error);
  ROS_INFO_STREAM("Angular Error: " << angular_error);

  return true;
}

bool Docking::approach()
{
  ros::Rate loop_rate(10);
  int count = 0;
  while (ros::ok())
  {
    try
    {
      tf_listener_.lookupTransform(base_frame_, tag_frame_, ros::Time(0),
                                   last_tag_);
    }
    catch (tf::TransformException ex)
    {
      ROS_ERROR("%s", ex.what());
      ros::Duration(1.0).sleep();

      count++;
      if (count > 3)
      {
        ROS_WARN("Start Recovery");
        if (!recovery())
          return false;
      }

      continue;
    }

    // TO BE CHECKED: trasforma la tf del tag nello stesso frame della base
    // (risolve problemi di rotazione)
    tf::Quaternion rot = tf::createQuaternionFromRPY(0.0, M_PI_2, M_PI);

    double yaw = tf::getYaw(last_tag_.getRotation() * rot);

    double delta_x = last_tag_.getOrigin().x() - alignment_distance_;
    double delta_y = last_tag_.getOrigin().y();   
    
    if (fabs(delta_x) < alignment_tolerance_ &&
        fabs(delta_y) < alignment_tolerance_ &&
        fabs(yaw) < alignment_tolerance_)
    {
      publishZeroVelocity();
      return true;
    }
    geometry_msgs::Twist velocity_msg;
    velocity_msg.linear.x = std::min(std::max(0.25 * delta_x, -0.25), 0.25);
    velocity_msg.linear.y = std::min(std::max(0.25 * delta_y, -0.25), 0.25);
    velocity_msg.angular.z = std::min(std::max(0.25 * yaw, -0.10), 0.10);

	ROS_INFO_STREAM("tag <x: "   << last_tag_.getOrigin().x() << " y: " << last_tag_.getOrigin().y() << " yaw: " << yaw <<"> "
				 << "delta [" << alignment_tolerance_<<"] <x: " << delta_x << " y: " << delta_x << " yaw: " << yaw  <<"> "
				 << "vel x: " << velocity_msg.linear.x << " y: " << velocity_msg.linear.y << " yaw: " << velocity_msg.angular.z  <<"> " );
        
    // ROS_INFO_STREAM("Velocity: " << velocity_msg);

    velocity_pub_.publish(velocity_msg);

    loop_rate.sleep();
  }
}

bool Docking::docking()
{
  ros::Rate loop_rate(10);

  double linear_error, angular_error;

  while (ros::ok())
  {
    loop_rate.sleep();

    if (!alignmentError(linear_error, angular_error))
    {
      publishZeroVelocity();
      continue;
    }

    if (fabs(linear_error) < 0.003 && fabs(angular_error) < 0.01)
    {
      publishZeroVelocity();
      return true;
    }

    geometry_msgs::Twist velocity_msg;

    velocity_msg.linear.x = std::min(
        std::max(0.5 * linear_error * sin(angular_error), -0.25), 0.25);
    velocity_msg.linear.y = std::min(
        std::max(0.5 * linear_error * cos(angular_error), -0.25), 0.25);
    velocity_msg.angular.z =
        std::min(std::max(0.5 * angular_error, -0.25), 0.25);
    //ROS_INFO_STREAM("Velocity: " << velocity_msg);

    velocity_pub_.publish(velocity_msg);
  }
}

bool Docking::recovery()
{
  if (tf_listener_.frameExists(tag_frame_))
    return true;

  for (auto i = 0; i < 12; i++)
  {
    if (i % 3 == 0)
      driveForwardOdom(0.10);

    rotateOdom(M_PI / 6.0);
    if (tf_listener_.frameExists(tag_frame_))
      return true;
  }

  return false;
}

bool Docking::rotateOdom(double angle)
{
  // wait for the listener to get the first message
  tf_listener_.waitForTransform(base_frame_, odom_frame_, ros::Time(0),
                                ros::Duration(1.0));

  // we will record transforms here
  tf::StampedTransform start_transform;
  tf::StampedTransform current_transform;

  // record the starting transform from the odometry to the base frame
  tf_listener_.lookupTransform(base_frame_, odom_frame_, ros::Time(0),
                               start_transform);

  // we will be sending commands of type "twist"
  geometry_msgs::Twist base_cmd;
  // the command will be to rotate at 0.25 m/s
  base_cmd.linear.x = base_cmd.linear.y = 0;
  if (angle > 0.0)
    base_cmd.angular.z = 0.1;
  else
    base_cmd.angular.z = -0.1;

  ros::Rate rate(10.0);
  while (nh_.ok())
  {
    // send the drive command
    velocity_pub_.publish(base_cmd);

    rate.sleep();
    // get the current transform
    try
    {
      tf_listener_.lookupTransform(base_frame_, odom_frame_, ros::Time(0),
                                   current_transform);
    }
    catch (tf::TransformException ex)
    {
      ROS_ERROR("%s", ex.what());
      return false;
    }
    // see how far we've traveled
    tf::Transform relative_transform =
        start_transform.inverse() * current_transform;

    tf::Matrix3x3 rot_mat(relative_transform.getRotation());
    double roll, pitch, yaw;
    rot_mat.getRPY(roll, pitch, yaw);

    ROS_INFO_STREAM("Yaw: " << yaw << "Angle: " << angle);

    if (fabs(yaw) > fabs(angle))
      return true;
  }
}

bool Docking::driveForwardOdom(double distance)
{
  // wait for the listener to get the first message
  tf_listener_.waitForTransform(base_frame_, odom_frame_, ros::Time(0),
                                ros::Duration(1.0));

  // we will record transforms here
  tf::StampedTransform start_transform;
  tf::StampedTransform current_transform;

  // record the starting transform from the odometry to the base frame
  tf_listener_.lookupTransform(base_frame_, odom_frame_, ros::Time(0),
                               start_transform);

  // we will be sending commands of type "twist"
  geometry_msgs::Twist base_cmd;
  // the command will be to go forward at 0.25 m/s
  base_cmd.linear.y = base_cmd.angular.z = 0;
  base_cmd.linear.x = 0.25;

  ros::Rate rate(10.0);
  while (nh_.ok())
  {
    // send the drive command
    velocity_pub_.publish(base_cmd);

    rate.sleep();
    // get the current transform
    try
    {
      tf_listener_.lookupTransform(base_frame_, odom_frame_, ros::Time(0),
                                   current_transform);
    }
    catch (tf::TransformException ex)
    {
      ROS_ERROR("%s", ex.what());
      return false;
    }
    // see how far we've traveled
    tf::Transform relative_transform =
        start_transform.inverse() * current_transform;
    double dist_moved = relative_transform.getOrigin().length();

    if (dist_moved > distance)
    {
      publishZeroVelocity();
      return true;
    }
  }
}

void Docking::publishZeroVelocity()
{
  geometry_msgs::Twist cmd_vel;
  cmd_vel.linear.x = 0.0;
  cmd_vel.linear.y = 0.0;
  cmd_vel.angular.z = 0.0;
  velocity_pub_.publish(cmd_vel);
}

void Docking::showLines(double a, double b, double c, int id)
{

  visualization_msgs::Marker line_list;
  line_list.header.frame_id = "/base_footprint";
  line_list.header.stamp = ros::Time::now();
  line_list.ns = "lines";
  line_list.action = visualization_msgs::Marker::ADD;
  line_list.pose.orientation.w = 1.0;
  line_list.id = id;
  line_list.type = visualization_msgs::Marker::LINE_LIST;
  line_list.scale.x = 0.1;
  line_list.color.g = 1.0;
  line_list.color.a = 1.0;

  geometry_msgs::Point p;
  p.x = 0;
  p.y = -c / b;
  p.z = 0;
  line_list.points.push_back(p);

  p.x = 1;
  p.y = (-c - a) / b;
  line_list.points.push_back(p);

  marker_pub_.publish(line_list);
  ros::spinOnce();
}
}
