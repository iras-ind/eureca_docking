#include <ros/ros.h>

#include <eureca_docking/eureca_docking.h>

int main(int argc, char** argv)
{
  ros::init(argc, argv, "eureca_docking_node");
  ros::NodeHandle nh;

  // ros::AsyncSpinner spinner(0);
  // spinner.start();

  ROS_INFO("Docking Node Ready");

  eureca::Docking docking;
  ros::spin();

  return (0);

  // spinner.stop();
}
